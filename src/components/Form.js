import React, { useContext } from "react";

import {
  FormControl,
  InputLabel,
  Input,
  Button,
  Box,
  makeStyles,
} from "@material-ui/core";
import { ConversionContext } from "../hooks/useHook";

import InputFromTo from "./InputFromTo";
import CompareArrowsIcon from "@material-ui/icons/CompareArrows";

const useStyles = makeStyles((theme) => ({
  form: {
    display: "flex",
    justifyContent: "center",
    flexDirection: "column",
    [theme.breakpoints.up("md")]: {
      flexDirection: "row",
    },
  },
  amount: {
    marginRight: theme.spacing(3),
  },
}));

export default function Form() {
  const { state, handleChange, handleSwitch, handleSubmit } =
    useContext(ConversionContext);

  const classes = useStyles();
  return (
    <Box>
      <form className={classes.form}>
        <FormControl className={classes.amount}>
          <InputLabel>Amount</InputLabel>
          <Input onBlur={handleChange} name="amount" />
        </FormControl>

        <InputFromTo title={"from"} value={state.from} />

        <Button onClick={handleSwitch}>
          <CompareArrowsIcon />
        </Button>

        <InputFromTo title={"to"} value={state.to} />

        <Button onClick={handleSubmit} variant="contained" color="primary">
          Primary
        </Button>
      </form>
    </Box>
  );
}
