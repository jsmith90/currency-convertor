import React, { useContext, Suspense } from "react";
import { Box, makeStyles } from "@material-ui/core";
import { ConversionContext } from "../hooks/useHook";

const useStyles = makeStyles(() => ({
  font: {
    fontSize: "3rem",
  },
  green: {
    color: "#b3d56b",
  },
}));

export default function ConversionDefault() {
  const { state } = useContext(ConversionContext);
  const classes = useStyles();

  return (
    <Suspense fallback={<span>loading...</span>}>
      <Box>
        <div className={classes.font}>
          <span>{`${state.amount}${state.from}=`}</span>{" "}
          <span className={classes.green}>{`${state?.total} ${state.to}`}</span>
        </div>

        <Box>{`1${state.from} = ${state.rate} ${state.to}`}</Box>
      </Box>
      <Box></Box>
    </Suspense>
  );
}
