import React from "react";
import { FormControl, InputLabel, Input } from "@material-ui/core";

const InputFromTo = ({ title, handler, value }) => (
  <FormControl>
    <InputLabel>{title ? title : ""}</InputLabel>
    <Input
      value={value && value}
      onBlur={handler && handler}
      name={title && title}
    />
  </FormControl>
);

export default InputFromTo;
