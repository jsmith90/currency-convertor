import { Box, Typography, makeStyles, Container } from "@material-ui/core";
import React from "react";

import Header from "./Header";
const useStyles = makeStyles((theme) => ({
  "@global": {
    ".MuiBox-root": {
      margin: theme.spacing(2),
      padding: theme.spacing(2),
    },
    html: {
      background: "#f2f2f2",
    },
  },
}));

const MainText = ({ title }) => (
  <Box>
    <Typography component="h2" variant="h2">
      {title}
    </Typography>
  </Box>
);
export default function Layout({ children }) {
  useStyles();
  return (
    <div>
      <Header />
      <main>
        <Container className="App" maxWidth="lg">
          <MainText title="I want to convert" />
          {children}
        </Container>
      </main>
    </div>
  );
}
