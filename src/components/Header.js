import React from "react";
import { AppBar, Toolbar, Typography, makeStyles } from "@material-ui/core";
import logo from "../assets/logo.svg";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#fff",
    marginBottom: theme.spacing(10),
  },
  subTitle: {
    textTransform: "upperCase",
    fontSize: "0.7em",
  },
}));

const Header = () => {
  const classes = useStyles();
  return (
    <AppBar position="static" className={classes.root}>
      <Toolbar>
        <img src={logo} alt="Logo" width="100px" />
        <Typography component="h1" variant="h3" color="primary">
          <span className={classes.subTitle}>Currency</span> Exchange
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
