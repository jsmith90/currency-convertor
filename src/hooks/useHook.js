import { createContext, useEffect, useReducer } from "react";
import Nomics from "nomics";

const apiKey = "b371fd3c3dc386cc0ef2244b255fab80b1e7c4bab";
const nomics = new Nomics({
  apiKey: apiKey,
});

const initialState = {
  amount: "0",
  from: "BTC",
  to: "ETH",
  total: "0",
  rate: "",
};

const actions = {
  AMOUNT: "AMOUNT",
  SWITCH: "SWITCH",
  CONVERT: "CONVERT",
};

function reducer(state, action) {
  const { value } = action;
  switch (action.type) {
    case actions.AMOUNT:
      return {
        ...state,
        amount: value,
      };
    case actions.SWITCH:
      return {
        ...state,
        from: state.to,
        to: state.from,
      };
    case actions.CONVERT:
      return {
        ...state,
        total: value.total,
        rate: value.rate,
      };
    default:
      throw new Error();
  }
}

export default function useHook() {
  const [state, dispatch] = useReducer(reducer, initialState);

  const handleChange = async (event) => {
    dispatch({
      type: actions.AMOUNT,
      value: event.target.value,
    });
  };

  const callConversion = async () => {
    let result;
    try {
      const currencies = await nomics.currenciesTicker({
        interval: ["1d"],
        ids: [state.from],
        convert: state.to,
      });

      result = {
        rate: currencies[0].price,
        total: state.amount * currencies[0].price,
      };

      return result;
    } catch {
      result = {
        total: "Error has occured, please try again",
      };
      return result;
    }
  };

  const setConversion = async () => {
    const result = await callConversion();
    dispatch({
      type: actions.CONVERT,
      value: {
        total: result.total,
        rate: result.rate,
      },
    });
  };

  const handleSubmit = (event) => {
    event?.preventDefault();
    setConversion();
  };

  const handleSwitch = async () => {
    dispatch({
      type: actions.SWITCH,
      value: {
        from: state.to,
        to: state.from,
      },
    });
    setConversion();
  };

  useEffect(() => {
    setConversion();
  }, [dispatch, state.from, state.to]);

  return {
    state,
    handleSwitch,
    handleSubmit,
    handleChange,
  };
}

export const ConversionContext = createContext({});
