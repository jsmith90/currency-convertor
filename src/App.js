import React from "react";
import "./styles/App.css";
import Form from "./components/Form";

import Layout from "./components/Layout";
import useHook, { ConversionContext } from "./hooks/useHook";
import ConversionDefault from "./components/ConversionResult";

function App() {
  const data = useHook();

  return (
    <Layout>
      <ConversionContext.Provider value={data}>
        <Form />
        <ConversionDefault />
      </ConversionContext.Provider>
    </Layout>
  );
}

export default App;
