import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders initial text", () => {
  render(<App />);
  const linkElement = screen.getByText("I want to convert");
  expect(linkElement).toBeInTheDocument();
});
